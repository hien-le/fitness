import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';

import SafeAreaView from 'react-native-safe-area-view';

import styles from './Styles/GoalScreenStyle';
import {Images} from '../../Themes';
import FastImage from 'react-native-fast-image';

const GoalScreen = ({navigation}) => {
  const onPressNutrition = () => navigation.navigate('NutritionScreen');

  const onPressPractice = () => navigation.navigate('PracticeScreen');

  const onPressShare = () => navigation.navigate('ShareScreen');

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <FastImage
          source={{
            uri:
              'https://res.cloudinary.com/namlaem/image/upload/v1600431381/Fitness/background01_pyukgt.webp',
            priority: FastImage.priority.normal,
          }}
          style={styles.background}
        />

        <View style={styles.viewContent}>
          <View style={styles.viewHeader}>
            <Text numberOfLines={1} style={styles.textTitle}>
              Giảm cân
            </Text>
          </View>
          <View style={styles.viewChoose}>
            <TouchableOpacity style={[styles.button]} onPress={onPressPractice}>
              <Text numberOfLines={1} style={styles.text}>
                Tập luyện
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.button]}
              onPress={onPressNutrition}>
              <Text numberOfLines={1} style={styles.text}>
                Chế độ dinh dưỡng
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.button]} onPress={onPressShare}>
              <Text numberOfLines={1} style={styles.text}>
                Cộng đồng chia sẻ
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={styles.viewBack}>
        <TouchableOpacity
          style={styles.buttonBack}
          onPress={() => navigation.goBack()}>
          <Image
            source={Images.back}
            resizeMode="contain"
            style={styles.iconBack}
          />
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default GoalScreen;
