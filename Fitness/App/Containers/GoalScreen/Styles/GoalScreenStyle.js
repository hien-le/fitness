import {StyleSheet} from 'react-native';
import {Colors, Metrics, Fonts, Normalize} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    flex: 1,
    padding: Metrics.doubleBaseMargin,
  },
  viewHeader: {
    flex: 0.4,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: Metrics.doubleBaseMargin,
  },
  textTitle: {
    color: Colors.white,
    fontSize: Normalize(56),
    fontFamily: Fonts.upperCase,
    textTransform: 'uppercase',
    marginBottom: Metrics.smallMargin,
  },
  viewChoose: {
    flex: 0.6,
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: Metrics.quadrupleBaseMargin,
    paddingHorizontal: Metrics.doubleBaseMargin,
  },
  button: {
    width: Metrics.screenWidth / 1.6,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    height: Metrics.screenHeight / 15,
    borderRadius: Metrics.borderRadiusSmall,
    marginBottom: Metrics.tripleBaseMargin,
  },
  text: {
    color: Colors.black,
    fontSize: Normalize(28),
    fontFamily: Fonts.upperCase,
    textTransform: 'uppercase',
    marginBottom: Metrics.smallMargin,
  },
  viewContent: {
    width: '100%',
    height: '100%',
    position: 'absolute',
  },
  viewBack: {
    position: 'absolute',
    top: Metrics.tripleBaseMargin,
  },
  buttonBack: {
    alignItems: 'center',
    justifyContent: 'center',
    width: Metrics.screenWidth / 7,
    height: Metrics.screenHeight / 18,
  },
  iconBack: {
    tintColor: Colors.white,
    width: 35,
    height: 30,
  },
});
