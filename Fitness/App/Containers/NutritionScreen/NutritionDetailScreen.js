import React, {useState} from 'react';
import {View, Text, Image, TouchableOpacity, FlatList} from 'react-native';

import SafeAreaView from 'react-native-safe-area-view';
import FastImage from 'react-native-fast-image';
import dataNutrition from '../../../data';

import styles from './Styles/NutritionDetailScreenStyle';
import {Images} from '../../Themes';

const NutritionDetailScreen = ({navigation, route}) => {
  const onPressBack = () => navigation.goBack();

  const renderNutrition = ({item, index}) => {
    if (item.day === route.params.day) {
      return (
        <View key={'nutrition' + index} style={styles.viewNutrition}>
          <View style={styles.viewMealName}>
            <Text style={styles.textMealName}>Sáng</Text>
          </View>
          {item.morning.map((mor, indexMor) => {
            return (
              <View key={'mor' + indexMor} style={styles.viewMealDetail}>
                <Text style={styles.textContent}>{mor}</Text>
              </View>
            );
          })}
          <View style={styles.viewMealName}>
            <Text style={styles.textMealName}>Trưa</Text>
          </View>
          {item.lunch.map((lun, indexLun) => {
            return (
              <View key={'lun' + indexLun} style={styles.viewMealDetail}>
                <Text style={styles.textContent}>{lun}</Text>
              </View>
            );
          })}
          <View style={styles.viewMealName}>
            <Text style={styles.textMealName}>Chiều</Text>
          </View>
          {item.dinner.map((din, indexDin) => {
            return (
              <View key={'din' + indexDin} style={styles.viewMealDetail}>
                <Text style={styles.textContent}>{din}</Text>
              </View>
            );
          })}
          <View style={styles.viewMealName}>
            <Text style={styles.textMealName}>Tối</Text>
          </View>
          {item.night.map((nig, indexNig) => {
            return (
              <View key={'nig' + indexNig} style={styles.viewMealDetail}>
                <Text style={styles.textContent}>{nig}</Text>
              </View>
            );
          })}
          <View style={styles.viewTotal}>
            <Text style={styles.textTotal}>{item.total}</Text>
          </View>
        </View>
      );
    }
  };

  return (
    <SafeAreaView style={styles.container} forceInset={{top: 'never'}}>
      <View style={styles.viewHeader}>
        <Text numberOfLines={1} style={styles.textHeader}>
          Thực đơn {route.params.day}
        </Text>
        <View style={styles.viewBack}>
          <TouchableOpacity style={styles.buttonBack} onPress={onPressBack}>
            <Image
              source={Images.back}
              resizeMode="contain"
              style={styles.iconBack}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.viewContent}>
        <FastImage
          source={{
            uri:
              'https://res.cloudinary.com/namlaem/image/upload/v1600431383/Fitness/background14_o4ecoz.webp',
            priority: FastImage.priority.high,
          }}
          style={styles.background}
        />
        <FlatList
          style={styles.viewContentMale}
          showsVerticalScrollIndicator={false}
          data={dataNutrition}
          extraData={dataNutrition}
          renderItem={renderNutrition}
          keyExtractor={(item, index) => 'keyNutrition' + index}
        />
      </View>
    </SafeAreaView>
  );
};

export default NutritionDetailScreen;
