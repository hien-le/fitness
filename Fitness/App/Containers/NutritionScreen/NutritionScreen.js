import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';

import SafeAreaView from 'react-native-safe-area-view';
import FastImage from 'react-native-fast-image';

import styles from './Styles/NutritionScreenStyle';
import {Images} from '../../Themes';

const NutritionScreen = ({navigation}) => {
  const onPressBack = () => navigation.goBack();

  const onPressDay = (day) => () =>
    navigation.navigate('NutritionDetailScreen', {day});

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <FastImage
          source={{
            uri:
              'https://res.cloudinary.com/namlaem/image/upload/v1600431381/Fitness/background04_n2nohv.webp',
            priority: FastImage.priority.high,
          }}
          style={styles.background}
        />

        <View style={styles.viewContent}>
          <View style={styles.viewHeader}>
            <Text numberOfLines={1} style={styles.textTitle}>
              Chế độ dinh dưỡng
            </Text>
          </View>
          <View style={styles.viewChoose}>
            <TouchableOpacity
              style={[styles.button]}
              onPress={onPressDay('Ngày 1')}>
              <Text numberOfLines={1} style={styles.text}>
                Thực đơn ngày 1
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.button]}
              onPress={onPressDay('Ngày 2')}>
              <Text numberOfLines={1} style={styles.text}>
                Thực đơn ngày 2
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.button]}
              onPress={onPressDay('Ngày 3')}>
              <Text numberOfLines={1} style={styles.text}>
                Thực đơn ngày 3
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={styles.viewBack}>
        <TouchableOpacity
          style={styles.buttonBack}
          onPress={() => navigation.goBack()}>
          <Image
            source={Images.back}
            resizeMode="contain"
            style={styles.iconBack}
          />
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default NutritionScreen;
