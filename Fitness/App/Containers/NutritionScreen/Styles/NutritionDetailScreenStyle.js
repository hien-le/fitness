import {StyleSheet} from 'react-native';
import {Colors, Metrics, Fonts, Normalize} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Metrics.doubleBaseMargin,
  },
  viewHeader: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Colors.silver,
  },
  viewBack: {
    position: 'absolute',
    left: Metrics.halfTripleBaseMargin,
  },
  buttonBack: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    width: Metrics.screenWidth / 7,
    height: Metrics.screenHeight / 20,
  },
  iconBack: {
    tintColor: Colors.black,
    width: 35,
    height: 30,
  },
  viewMealName: {
    backgroundColor: Colors.white,
    width: Metrics.screenWidth / 4,
    height: Metrics.screenHeight / 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopRightRadius: Metrics.doubleBorderRadiusSmall,
    marginVertical: Metrics.baseMargin,
  },
  viewTotal: {
    backgroundColor: Colors.white,
    width: '80%',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: Metrics.baseMargin,
    alignSelf: 'center',
    padding: Metrics.smallMargin,
    borderRadius: Metrics.borderRadiusSmall,
  },
  textTotal: {
    color: Colors.black,
    fontSize: Normalize(22),
    fontFamily: Fonts.upperCase,
    textAlign: 'center',
  },
  background: {
    flex: 1,
  },
  textMealName: {
    color: Colors.black,
    fontSize: Normalize(25),
    fontFamily: Fonts.upperCase,
  },
  viewContent: {
    flex: 0.9,
  },
  viewContentMale: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  viewMealDetail: {
    width: '100%',
    backgroundColor: Colors.blackOpacity,
    padding: Metrics.smallMargin,
  },
  textHeader: {
    color: Colors.black,
    fontSize: Normalize(28),
    fontFamily: Fonts.upperCase,
    textAlign: 'center',
  },
  textContent: {
    color: Colors.white,
    marginLeft: Metrics.baseMargin,
    fontSize: Normalize(18),
    fontFamily: Fonts.regular,
  },
});
