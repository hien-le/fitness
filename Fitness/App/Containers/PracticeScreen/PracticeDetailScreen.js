import React, {useState} from 'react';
import {View, Text, Image, TouchableOpacity, Alert} from 'react-native';

import SafeAreaView from 'react-native-safe-area-view';
import Video from 'react-native-video';

import styles from './Styles/PracticeScreenStyle';
import {Images} from '../../Themes';
import FastImage from 'react-native-fast-image';
import {ScrollView} from 'react-native-gesture-handler';

const PracticeScreen = ({navigation, route}) => {
  const [selected, setSelected] = useState(0);

  const onPressBack = () => navigation.goBack();

  return (
    <SafeAreaView style={styles.container} forceInset={{top: 'never'}}>
      <View style={styles.viewHeader}>
        <Text numberOfLines={1} style={styles.textHeader}>
          {route.params.title}
        </Text>
        <View style={styles.viewBack}>
          <TouchableOpacity style={styles.buttonBack} onPress={onPressBack}>
            <Image
              source={Images.back}
              resizeMode="contain"
              style={styles.iconBack}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.viewContent}>
        <Video
          source={{
            uri:
              route.params.title === '10 phút hit đốt mỡ'
                ? 'https://res.cloudinary.com/namlaem/video/upload/v1600436935/Fitness/burnFatMale_1_uksgzq.mp4'
                : 'https://res.cloudinary.com/namlaem/video/upload/v1600436923/Fitness/burnFatFemale_1_gzpnw7.mp4',
          }}
          // source={
          //   route.params.title === '10 phút hit đốt mỡ'
          //     ? require('../../Images/burnFatMale.mp4')
          //     : require('../../Images/burnFatFemale.mp4')
          // }
          resizeMode="contain"
          onError={() => Alert.alert('Have an error, please try again later !')}
          poster="https://i.gifer.com/YCZH.gif"
          posterResizeMode="contain"
          style={styles.video}
          controls={true}
        />
      </View>
    </SafeAreaView>
  );
};

export default PracticeScreen;
