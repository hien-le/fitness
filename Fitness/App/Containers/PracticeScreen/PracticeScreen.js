import React, {useState} from 'react';
import {View, Text, Image, TouchableOpacity, Alert} from 'react-native';

import SafeAreaView from 'react-native-safe-area-view';
import Video from 'react-native-video';

import styles from './Styles/PracticeScreenStyle';
import {Images} from '../../Themes';
import FastImage from 'react-native-fast-image';
import {ScrollView} from 'react-native-gesture-handler';

const PracticeScreen = ({navigation}) => {
  const dataPractice = [
    {
      title: '10 phút hit đốt mỡ',
      background:
        'https://res.cloudinary.com/namlaem/image/upload/v1600360873/Fitness/background11_vsogwp.jpg',
    },
    {
      title: 'Bài tập đốt mỡ giảm cân',
      background:
        'https://res.cloudinary.com/namlaem/image/upload/v1600360940/Fitness/background12_p656ab.jpg',
    },
    {
      title: 'Bài tập đốt mỡ bụng',
      background:
        'https://res.cloudinary.com/namlaem/image/upload/v1600360941/Fitness/background13_nnht0n.jpg',
    },
  ];
  const [selected, setSelected] = useState(0);

  const onPressBack = () => navigation.goBack();

  return (
    <SafeAreaView style={styles.container} forceInset={{top: 'never'}}>
      <View style={styles.viewHeader}>
        <Text numberOfLines={1} style={styles.textHeader}>
          Tập luyện
        </Text>
        <View style={styles.viewBack}>
          <TouchableOpacity style={styles.buttonBack} onPress={onPressBack}>
            <Image
              source={Images.back}
              resizeMode="contain"
              style={styles.iconBack}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.viewMenu}>
        <TouchableOpacity style={styles.buttonMenu}>
          <Text style={[styles.textMenu, styles.textMenuChoose]}>Bài tập</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonMenu}
          onPress={() => Alert.alert('Coming soon !')}>
          <Text style={styles.textMenu}>Tiến độ</Text>
        </TouchableOpacity>
      </View>
      <ScrollView style={styles.viewContent}>
        {dataPractice.map((item, index) => {
          return (
            <TouchableOpacity
              key={'key' + index}
              onPress={() =>
                navigation.navigate('PracticeDetailScreen', {title: item.title})
              }>
              <FastImage
                source={{
                  uri: item.background,
                  priority: FastImage.priority.normal,
                }}
                style={styles.background}
              />
              <View style={styles.viewTextContent}>
                <Text numberOfLines={1} style={styles.textContent}>
                  {item.title}
                </Text>
              </View>
            </TouchableOpacity>
          );
        })}

        {/* <Video
          source={require('../../Images/burnFatFemale.mp4')}
          resizeMode="contain"
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
          }}
          controls={true}
        /> */}
      </ScrollView>
    </SafeAreaView>
  );
};

export default PracticeScreen;
