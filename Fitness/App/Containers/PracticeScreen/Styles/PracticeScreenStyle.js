import {StyleSheet} from 'react-native';
import {Colors, Metrics, Fonts, Normalize} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Metrics.doubleBaseMargin,
  },
  viewHeader: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewBack: {
    position: 'absolute',
    left: Metrics.halfTripleBaseMargin,
  },
  buttonBack: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    width: Metrics.screenWidth / 7,
    height: Metrics.screenHeight / 20,
  },
  iconBack: {
    tintColor: Colors.black,
    width: 35,
    height: 30,
  },
  viewMenu: {
    flex: 0.1,
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '70%',
    flexDirection: 'row',
    alignSelf: 'center',
  },
  buttonMenu: {
    padding: Metrics.smallMargin,
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  textMenuChoose: {
    color: Colors.black,
  },
  textMenu: {
    color: Colors.silver,
    fontSize: Normalize(25),
    fontFamily: Fonts.upperCase,
    textAlign: 'center',
  },
  viewContent: {
    flex: 0.9,
    borderTopWidth: 1,
    borderTopColor: Colors.silver,
  },
  background: {
    flex: 1,
    height: Metrics.screenHeight / 3,
    marginTop: Metrics.baseMargin,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewTextContent: {
    width: '100%',
    height: Metrics.screenHeight / 3,
    top: 10,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContent: {
    color: Colors.white,
    fontSize: Normalize(25),
    fontFamily: Fonts.upperCase,
    textTransform: 'uppercase',
  },
  textHeader: {
    color: Colors.black,
    fontSize: Normalize(28),
    fontFamily: Fonts.upperCase,
    textAlign: 'center',
  },
  video: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});
