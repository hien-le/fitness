import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  Alert,
} from 'react-native';

import SafeAreaView from 'react-native-safe-area-view';
import FastImage from 'react-native-fast-image';
import dataNutrition from '../../../data';

import styles from './Styles/ShareScreenStyle';
import {Images} from '../../Themes';

const ShareScreen = ({navigation, route}) => {
  const onPressBack = () => navigation.goBack();

  return (
    <SafeAreaView style={styles.container} forceInset={{top: 'never'}}>
      <View style={styles.viewHeader}>
        <Text numberOfLines={1} style={styles.textHeader}>
          Cộng đồng chia sẻ
        </Text>
        <View style={styles.viewBack}>
          <TouchableOpacity style={styles.buttonBack} onPress={onPressBack}>
            <Image
              source={Images.back}
              resizeMode="contain"
              style={styles.iconBack}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.viewContent}>
        <FastImage
          source={{
            uri:
              'https://res.cloudinary.com/namlaem/image/upload/v1600441765/Fitness/background15_ecjrha.png',
            priority: FastImage.priority.normal,
          }}
          style={styles.background}
        />
        <View style={styles.viewShare}>
          <View style={styles.buttonIcon}>
            <View style={styles.viewIcon}>
              <Image
                source={Images.heart}
                resizeMode="contain"
                style={styles.icon}
              />
              <Text style={styles.textIcon}>999</Text>
            </View>
            <View style={styles.viewIcon}>
              <Image
                source={Images.comment}
                resizeMode="contain"
                style={styles.icon}
              />
              <Text style={styles.textIcon}>99</Text>
            </View>
            <View style={styles.viewIcon}>
              <Image
                source={Images.share}
                resizeMode="contain"
                style={styles.icon}
              />
              <Text style={styles.textIcon}>Share</Text>
            </View>
          </View>
          <TouchableOpacity
            style={styles.buttonShare}
            onPress={() => Alert.alert('Click Share !')}>
            <Text style={styles.textShare}>Chia sẻ ngay</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ShareScreen;
