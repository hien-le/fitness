import React from 'react';
import {View, Text, ImageBackground, TouchableOpacity} from 'react-native';

import SafeAreaView from 'react-native-safe-area-view';

import styles from './Styles/StartScreenStyle';
import {Images} from '../../Themes';

const StartScreen = ({navigation}) => {
  const onPressTrial = () => navigation.navigate('TrialStack');
  const onPressSignIn = () => navigation.navigate('AppStack');

  return (
    <SafeAreaView style={styles.container} forceInset={{top: 'never'}}>
      <ImageBackground source={Images.background01} style={styles.background}>
        <View style={styles.viewContent}>
          <Text numberOfLines={1} style={styles.textTitle}>
            Fitness
          </Text>
        </View>
        <View style={styles.viewButton}>
          <TouchableOpacity style={styles.buttonTrial} onPress={onPressTrial}>
            <Text numberOfLines={1} style={styles.textButtonTrial}>
              Học thử 3 ngày
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.buttonSignIn} onPress={onPressSignIn}>
            <Text numberOfLines={1} style={styles.textButtonSignIn}>
              Đăng nhập
            </Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
};

export default StartScreen;
