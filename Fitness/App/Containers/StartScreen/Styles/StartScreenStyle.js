import {StyleSheet} from 'react-native';
import {Colors, Metrics, Fonts, Normalize} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    flex: 1,
    padding: Metrics.doubleBaseMargin,
  },
  viewContent: {
    flex: 0.65,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textTitle: {
    color: Colors.white,
    fontSize: Normalize(56),
    fontFamily: Fonts.upperCase,
    textAlign: 'center',
    marginBottom: Metrics.quintupleBaseMargin,
    textTransform: 'uppercase',
  },
  viewButton: {
    flex: 0.35,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  buttonTrial: {
    width: Metrics.screenWidth / 2,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    height: Metrics.screenHeight / 15,
    borderRadius: Metrics.borderRadiusSmall,
    marginBottom: Metrics.doubleBaseMargin,
  },
  textButtonTrial: {
    color: Colors.black,
    fontSize: Normalize(28),
    fontFamily: Fonts.upperCase,
    textTransform: 'uppercase',
    marginBottom: Metrics.smallMargin,
  },
  buttonSignIn: {
    width: Metrics.screenWidth / 2,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    height: Metrics.screenHeight / 15,
    borderRadius: Metrics.borderRadiusSmall,
  },
  textButtonSignIn: {
    color: Colors.black,
    fontSize: Normalize(28),
    fontFamily: Fonts.upperCase,
    textTransform: 'uppercase',
    marginBottom: Metrics.smallMargin,
  },
});
