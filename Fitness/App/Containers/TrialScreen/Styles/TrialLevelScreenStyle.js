import {StyleSheet} from 'react-native';
import {Colors, Metrics, Fonts, Normalize} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    flex: 1,
    padding: Metrics.doubleBaseMargin,
  },

  viewHeader: {
    flex: 0.2,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  textTitle: {
    color: Colors.white,
    fontSize: Normalize(56),
    fontFamily: Fonts.upperCase,
    textTransform: 'uppercase',
    marginBottom: Metrics.smallMargin,
  },
  viewChooseLevel: {
    flex: 0.8,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: Metrics.quadrupleBaseMargin,
    paddingHorizontal: Metrics.doubleBaseMargin,
  },
  buttonChooseLevel: {
    width: Metrics.screenWidth / 2,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    height: Metrics.screenHeight / 15,
    borderRadius: Metrics.borderRadiusSmall,
    marginBottom: Metrics.tripleBaseMargin,
  },
  textChooseLevel: {
    color: Colors.black,
    fontSize: Normalize(28),
    fontFamily: Fonts.upperCase,
    textTransform: 'uppercase',
    marginBottom: Metrics.smallMargin,
  },
  viewBack: {
    position: 'absolute',
    top: Metrics.tripleBaseMargin,
  },
  buttonBack: {
    alignItems: 'center',
    justifyContent: 'center',
    width: Metrics.screenWidth / 7,
    height: Metrics.screenHeight / 18,
  },
  iconBack: {
    tintColor: Colors.white,
    width: 35,
    height: 30,
  },
});
