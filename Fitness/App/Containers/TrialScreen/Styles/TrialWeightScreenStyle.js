import {StyleSheet} from 'react-native';
import {Colors, Metrics, Fonts, Normalize} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    flex: 1,
    padding: Metrics.doubleBaseMargin,
  },
  viewHeader: {
    flex: 0.2,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  textTitle: {
    color: Colors.white,
    fontSize: Normalize(56),
    fontFamily: Fonts.upperCase,
    textTransform: 'uppercase',
    marginBottom: Metrics.smallMargin,
  },
  textUnit: {
    color: Colors.white,
    fontSize: Normalize(30),
    fontFamily: Fonts.upperCase,
  },
  viewChooseHeight: {
    flex: 0.7,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  wheelPicker: {
    width: '100%',
    height: Metrics.screenHeight / 2,
  },
  viewButton: {
    flex: 0.1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonNext: {
    flex: 0.9,
    height: Metrics.screenHeight / 15,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: Metrics.borderRadiusSmall,
  },
  textButtonNext: {
    color: Colors.black,
    fontSize: Normalize(20),
    fontFamily: Fonts.black,
    textTransform: 'uppercase',
  },
  viewBack: {
    position: 'absolute',
    top: Metrics.tripleBaseMargin,
  },
  buttonBack: {
    alignItems: 'center',
    justifyContent: 'center',
    width: Metrics.screenWidth / 7,
    height: Metrics.screenHeight / 18,
  },
  iconBack: {
    tintColor: Colors.white,
    width: 35,
    height: 30,
  },
});
