import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  SafeAreaView,
  Image,
} from 'react-native';

import styles from './Styles/TrialGenderScreenStyle';
import {Images} from '../../Themes';

const TrialGenderScreen = ({navigation, route}) => {
  const [gender, setGender] = useState('');

  const onPressNext = () => navigation.navigate('TrialHeightScreen');

  const onPressChooseGender = (genderChoose) => () => {
    setGender(genderChoose);
    onPressNext();
  };

  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground source={Images.background05} style={styles.background}>
        <View style={styles.viewHeader}>
          <Text numberOfLines={1} style={styles.textTitle}>
            Giới tính
          </Text>
        </View>
        <View style={styles.viewChooseGender}>
          <TouchableOpacity
            style={[styles.buttonGender]}
            onPress={onPressChooseGender('Male')}>
            <Text numberOfLines={1} style={styles.textGender}>
              Nam
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.buttonGender]}
            onPress={onPressChooseGender('Female')}>
            <Text numberOfLines={1} style={styles.textGender}>
              Nữ
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.buttonGender]}
            onPress={onPressChooseGender('Different')}>
            <Text numberOfLines={1} style={styles.textGender}>
              Khác
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.viewBack}>
          <TouchableOpacity
            style={styles.buttonBack}
            onPress={() => navigation.goBack()}>
            <Image
              source={Images.back}
              resizeMode="contain"
              style={styles.iconBack}
            />
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
};

export default TrialGenderScreen;
