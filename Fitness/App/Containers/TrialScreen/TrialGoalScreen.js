import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  SafeAreaView,
  Image,
} from 'react-native';

import styles from './Styles/TrialGoalScreenStyle';
import {Images} from '../../Themes';

const TrialGoalScreen = ({navigation, route}) => {
  const [goal, setGoal] = useState([]);

  const onChooseGoal = (goalChoose) => () => {
    setGoal(goalChoose);
    onPressNext();
  };

  const onPressNext = () => navigation.navigate('AppStack');
  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground source={Images.background05} style={styles.background}>
        <View style={styles.viewHeader}>
          <Text numberOfLines={1} style={styles.textTitle}>
            Mục tiêu
          </Text>
        </View>
        <View style={styles.viewChooseGoal}>
          <TouchableOpacity
            style={[styles.buttonChooseGoal]}
            onPress={onChooseGoal('Weight Loss')}>
            <Text numberOfLines={1} style={styles.textChooseGoal}>
              Giảm cân
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.buttonChooseGoal]}
            onPress={() => alert('Comming soon')}>
            <Text numberOfLines={1} style={styles.textChooseGoal}>
              Tăng cơ
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.buttonChooseGoal]}
            onPress={() => alert('Comming soon')}>
            <Text numberOfLines={1} style={styles.textChooseGoal}>
              Tăng mỡ
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.buttonChooseGoal]}
            onPress={() => alert('Comming soon')}>
            <Text numberOfLines={1} style={styles.textChooseGoal}>
              Giảm mỡ
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.viewBack}>
          <TouchableOpacity
            style={styles.buttonBack}
            onPress={() => navigation.goBack()}>
            <Image
              source={Images.back}
              resizeMode="contain"
              style={styles.iconBack}
            />
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
};

export default TrialGoalScreen;
