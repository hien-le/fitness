import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  SafeAreaView,
  Image,
} from 'react-native';

import {WheelPicker} from 'react-native-wheel-picker-android';

import {ProgressBar} from '../../Components';

import styles from './Styles/TrialHeightScreenStyle';
import {Images, Colors, Normalize, Fonts} from '../../Themes';

let arrayHeight = [];
for (let i = 130; i <= 230; i++) {
  arrayHeight.push(i.toString());
}

const TrialHeightScreen = ({navigation, route}) => {
  const [height, setHeight] = useState(0);

  const onChangeHeight = (index) => {
    setHeight(arrayHeight[index]);
  };

  const onPressNext = () => navigation.navigate('TrialWeightScreen');

  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground source={Images.background01} style={styles.background}>
        <View style={styles.viewHeader}>
          <Text numberOfLines={1} style={styles.textTitle}>
            Chiều cao
          </Text>
        </View>
        <View style={styles.viewChooseHeight}>
          <WheelPicker
            initPosition={40}
            data={arrayHeight}
            onItemSelected={onChangeHeight}
            isCyclic={true}
            selectedItemTextColor={Colors.white}
            selectedItemTextSize={Normalize(40)}
            selectedItemTextFontFamily={Fonts.black}
            itemTextColor={Colors.silver}
            itemTextFontFamily={Fonts.bold}
            itemTextSize={Normalize(35)}
            hideIndicator
            style={styles.wheelPicker}
          />
        </View>
        <View style={styles.viewButton}>
          <TouchableOpacity style={styles.buttonNext} onPress={onPressNext}>
            <Text numberOfLines={1} style={styles.textButtonNext}>
              Tiếp tục
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.viewBack}>
          <TouchableOpacity
            style={styles.buttonBack}
            onPress={() => navigation.goBack()}>
            <Image
              source={Images.back}
              resizeMode="contain"
              style={styles.iconBack}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.viewBack}>
          <TouchableOpacity
            style={styles.buttonBack}
            onPress={() => navigation.goBack()}>
            <Image
              source={Images.back}
              resizeMode="contain"
              style={styles.iconBack}
            />
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
};

export default TrialHeightScreen;
