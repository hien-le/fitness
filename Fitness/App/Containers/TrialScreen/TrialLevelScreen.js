import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  SafeAreaView,
  Image,
} from 'react-native';

import styles from './Styles/TrialLevelScreenStyle';
import {Images} from '../../Themes';

const TrialLevelScreen = ({navigation, route}) => {
  const [level, setLevel] = useState('');

  const onChooseLevel = (levelChoose) => () => {
    setLevel(levelChoose);
    onPressNext();
  };

  const onPressNext = () => navigation.navigate('TrialGoalScreen');

  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground source={Images.background01} style={styles.background}>
        <View style={styles.viewHeader}>
          <Text numberOfLines={1} style={styles.textTitle}>
            Trình độ
          </Text>
        </View>
        <View style={styles.viewChooseLevel}>
          <TouchableOpacity
            style={[styles.buttonChooseLevel]}
            onPress={onPressNext}>
            <Text numberOfLines={1} style={styles.textChooseLevel}>
              Người mới
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.buttonChooseLevel]}
            onPress={onPressNext}>
            <Text numberOfLines={1} style={styles.textChooseLevel}>
              Sơ cấp
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.buttonChooseLevel]}
            onPress={onPressNext}>
            <Text numberOfLines={1} style={styles.textChooseLevel}>
              Trung cấp
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.buttonChooseLevel]}
            onPress={onPressNext}>
            <Text numberOfLines={1} style={styles.textChooseLevel}>
              Cao cấp
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.viewBack}>
          <TouchableOpacity
            style={styles.buttonBack}
            onPress={() => navigation.goBack()}>
            <Image
              source={Images.back}
              resizeMode="contain"
              style={styles.iconBack}
            />
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
};

export default TrialLevelScreen;
