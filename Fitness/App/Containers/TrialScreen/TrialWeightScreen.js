import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  SafeAreaView,
  Image,
} from 'react-native';

import {WheelPicker} from 'react-native-wheel-picker-android';

import styles from './Styles/TrialWeightScreenStyle';
import {Images, Colors, Normalize, Fonts} from '../../Themes';

let arrayWeight = [];
for (let i = 40; i <= 130; i++) {
  arrayWeight.push(i.toString());
}

const TrialWeightScreen = ({navigation, route}) => {
  const [weight, setWeight] = useState(0);

  const onChangeWeight = (index) => {
    setWeight(arrayWeight[index]);
  };

  const onPressNext = () => navigation.navigate('TrialLevelScreen');

  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground source={Images.background05} style={styles.background}>
        <View style={styles.viewHeader}>
          <Text numberOfLines={1} style={styles.textTitle}>
            Cân nặng
          </Text>
        </View>
        <View style={styles.viewChooseHeight}>
          <WheelPicker
            initPosition={40}
            data={arrayWeight}
            onItemSelected={onChangeWeight}
            isCyclic={true}
            selectedItemTextColor={Colors.white}
            selectedItemTextSize={Normalize(40)}
            selectedItemTextFontFamily={Fonts.black}
            itemTextColor={Colors.silver}
            itemTextFontFamily={Fonts.bold}
            itemTextSize={Normalize(35)}
            hideIndicator
            style={styles.wheelPicker}
          />
        </View>
        <View style={styles.viewButton}>
          <TouchableOpacity style={styles.buttonNext} onPress={onPressNext}>
            <Text numberOfLines={1} style={styles.textButtonNext}>
              Tiếp tục
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.viewBack}>
          <TouchableOpacity
            style={styles.buttonBack}
            onPress={() => navigation.goBack()}>
            <Image
              source={Images.back}
              resizeMode="contain"
              style={styles.iconBack}
            />
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
};

export default TrialWeightScreen;
