import React from 'react';
import {Animated, StatusBar} from 'react-native';

// Libraries
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';

// Screens
import {StartScreen} from '../Containers/StartScreen';
import {
  TrialGenderScreen,
  TrialHeightScreen,
  TrialWeightScreen,
  TrialLevelScreen,
  TrialGoalScreen,
} from '../Containers/TrialScreen';
import {GoalScreen} from '../Containers/GoalScreen';
import {
  NutritionScreen,
  NutritionDetailScreen,
} from '../Containers/NutritionScreen';
import {
  PracticeScreen,
  PracticeDetailScreen,
} from '../Containers/PracticeScreen';
import {ShareScreen} from '../Containers/ShareScreen';

const forFade = ({current}) => ({
  cardStyle: {
    opacity: current.progress,
  },
});

const forSlide = ({current, next, inverted, layouts: {screen}}) => {
  const progress = Animated.add(
    current.progress.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    }),
    next
      ? next.progress.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 1],
          extrapolate: 'clamp',
        })
      : 0,
  );

  return {
    cardStyle: {
      transform: [
        {
          translateX: Animated.multiply(
            progress.interpolate({
              inputRange: [0, 1, 2],
              outputRange: [
                screen.width, // Focused, but offscreen in the beginning
                0, // Fully focused
                screen.width * -0.3, // Fully unfocused
              ],
              extrapolate: 'clamp',
            }),
            inverted,
          ),
        },
      ],
    },
  };
};

const TrialStack = createStackNavigator();
function TrialStacks() {
  return (
    <TrialStack.Navigator
      initialRouteName={'TrialGenderScreen'}
      headerMode="none">
      <TrialStack.Screen
        name={'TrialGenderScreen'}
        component={TrialGenderScreen}
        options={{
          gestureDirection: 'horizontal',
          cardStyleInterpolator: forSlide,
        }}
      />
      <TrialStack.Screen
        name={'TrialHeightScreen'}
        component={TrialHeightScreen}
        options={{
          gestureDirection: 'horizontal',
          cardStyleInterpolator: forSlide,
        }}
      />
      <TrialStack.Screen
        name={'TrialWeightScreen'}
        component={TrialWeightScreen}
        options={{
          gestureDirection: 'horizontal',
          cardStyleInterpolator: forSlide,
        }}
      />
      <TrialStack.Screen
        name={'TrialLevelScreen'}
        component={TrialLevelScreen}
        options={{
          gestureDirection: 'horizontal',
          cardStyleInterpolator: forSlide,
        }}
      />
    </TrialStack.Navigator>
  );
}
const AppStack = createStackNavigator();
function AppStacks() {
  return (
    <AppStack.Navigator initialRouteName={'TrialGoalScreen'} headerMode="none">
      <AppStack.Screen
        name={'TrialGoalScreen'}
        component={TrialGoalScreen}
        options={{
          gestureDirection: 'horizontal',
          cardStyleInterpolator: forSlide,
        }}
      />
      <AppStack.Screen
        name={'GoalScreen'}
        component={GoalScreen}
        options={{
          gestureDirection: 'horizontal',
          cardStyleInterpolator: forSlide,
        }}
      />
      <AppStack.Screen
        name={'NutritionScreen'}
        component={NutritionScreen}
        options={{
          gestureDirection: 'horizontal',
          cardStyleInterpolator: forSlide,
        }}
      />
      <AppStack.Screen
        name={'NutritionDetailScreen'}
        component={NutritionDetailScreen}
        options={{
          gestureDirection: 'horizontal',
          cardStyleInterpolator: forSlide,
        }}
      />
      <AppStack.Screen
        name={'PracticeScreen'}
        component={PracticeScreen}
        options={{
          gestureDirection: 'horizontal',
          cardStyleInterpolator: forSlide,
        }}
      />
      <AppStack.Screen
        name={'PracticeDetailScreen'}
        component={PracticeDetailScreen}
        options={{
          gestureDirection: 'horizontal',
          cardStyleInterpolator: forSlide,
        }}
      />
      <AppStack.Screen
        name={'ShareScreen'}
        component={ShareScreen}
        options={{
          gestureDirection: 'horizontal',
          cardStyleInterpolator: forSlide,
        }}
      />
    </AppStack.Navigator>
  );
}

const Stack = createStackNavigator();
export default function AppNavigation() {
  StatusBar.setBarStyle('light-content', true);
  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName={'StartScreen'}
          headerMode={'none'}
          screenOptions={{gestureEnabled: false}}
          mode={'modal'}>
          <Stack.Screen name={'StartScreen'} component={StartScreen} />
          <Stack.Screen
            name={'TrialStack'}
            component={TrialStacks}
            options={{
              gestureDirection: 'horizontal',
              cardStyleInterpolator: forSlide,
            }}
          />
          <Stack.Screen
            name={'AppStack'}
            component={AppStacks}
            options={{
              gestureDirection: 'horizontal',
              cardStyleInterpolator: forSlide,
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
}
