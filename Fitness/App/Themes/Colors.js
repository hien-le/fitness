const colors = {
  white: '#FFFFFF',
  black: '#000000',
  summerSky: '#1BA0E1',
  nobel: '#949494',
  dodgerBlue: '#1878F3',
  gainsboro: '#D8D8D8',
  aliceBlue: '#E7F3FF',
  gray: '#828282',
  blackOpacity: 'rgba(0, 0, 0, 0.2)',
  blackOpacity1: 'rgba(0, 0, 0, 0.7)',
  concrete: '#F2F2F2',
  silver: '#BDBDBD',
  royalBlue: '#2F80ED',
  persimmon: '#FF5555',
  mineShaft: '#333333',
  alto: '#E0E0E0',
  newYorkPink: '#DE8986',
  burntSienna: '#EB5757',
};

export default colors;
