export default {
  black: 'Roboto-Black',
  bold: 'Roboto-Bold',
  italic: 'Roboto-Italic',
  light: 'Roboto-Light',
  regular: 'Roboto-Regular',
  semiBold: 'Roboto-SemiBold',
  upperCase: 'UTM-Bebas',
};
