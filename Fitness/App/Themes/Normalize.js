import {RFValue} from 'react-native-responsive-fontsize';
const normalize = (size) => RFValue(size);
export default normalize;
